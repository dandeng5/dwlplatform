CREATE TABLE transactions (
  strategy varchar(255),
  symbol varchar(255),
  position varchar(255),
  quantity integer,
  price number(*, -2),
  timePlaced integer
);


INSERT ALL
  INTO transactions (strategy, symbol, position, quantity, price, timePlaced) VALUES ('MA', 'AAPL', 'BUY', 50, 110.94, 1531509201)
  INTO transactions (strategy, symbol, position, quantity, price, timePlaced) VALUES ('MA', 'SBUX', 'BUY', 140, 55.14, 1532569201)
  INTO transactions (strategy, symbol, position, quantity, price, timePlaced) VALUES ('MA', 'PH', 'BUY', 150, 310.34, 151506901)
  INTO transactions (strategy, symbol, position, quantity, price, timePlaced) VALUES ('MA', 'PH', 'SELL', 73, 119.67, 1531509881)
  INTO transactions (strategy, symbol, position, quantity, price, timePlaced) VALUES ('MA', 'MAT', 'BUY', 23, 56.91, 1531509761)
  INTO transactions (strategy, symbol, position, quantity, price, timePlaced) VALUES ('MA', 'NFLX', 'SELL', 88, 16.54, 1531589201)
SELECT * FROM dual;
