import static org.junit.Assert.assertEquals;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

import etcs.Price;
import etcs.Utility;

public class TestUtility {
	Utility val = new Utility();

    @Test
    public void buy() 
        throws Exception
    {    
    	String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        assertEquals("<trade>\r\n" + 
        		"  <buy>true</buy>\r\n" + 
        		"  <id>3</id>\r\n" + 
        		"  <price>15.03</price>\r\n" + 
        		"  <size>5000</size>\r\n" + 
        		"  <stock>AAPL</stock>\r\n" + 
        		"  <whenAsDate>"+time+"</whenAsDate>\r\n" + 
        		"</trade>", val.buy(15.03, 5000, "AAPL", 3));
    }
    
    @Test
    public void parser() 
        throws Exception
    {    
    	Price test = new Price("2018-07-12 10:55:00.06", 20.4070, 20.4877, 19.7920, 20.4877, 86521);
    	String line = "2018-07-12 10:55:00.06,20.4070,20.4877,19.7920,20.4877,86521";
        assertEquals(test.getHigh(), val.parse(line).getHigh(),0.01);
        assertEquals(test.getLow(), val.parse(line).getLow(),0.01);
        assertEquals(test.getClose(), val.parse(line).getClose(),0.01);
        assertEquals(test.getTimestamp(), val.parse(line).getTimestamp());

    }
}
