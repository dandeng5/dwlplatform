package strategies;
import java.util.Queue;

import etcs.Price;


public class TradingStrategy {
	
	public String tradingStrategy;
	public Queue<Price> prices;
	public int shortDuration;
	public int longDuration;
	public TwoMovingAveragesStrategy twoMA;
	
	public String decision;
	
	//set up constructor for Two MA
	public TradingStrategy(String tradingStrategy, Queue<Price> prices, int shortDuration, int longDuration) {
		this.setUpStrategy(tradingStrategy, prices, shortDuration, longDuration);
		this.twoMA = new TwoMovingAveragesStrategy();

	}
	
	public void setUpStrategy(String tradingStrategy, Queue<Price> prices, int shortDuration, int longDuration) {
		this.prices = prices;
		this.tradingStrategy = tradingStrategy;
		
		if (this.tradingStrategy.toUpperCase().equals("TWOMA")) {
			this.shortDuration = shortDuration;
			this.longDuration = longDuration;
		}
		else {
			System.out.println("Parameters do not match the requirement for a Two MA strategy");
		}
	}
	
	public String executeTwoMA(Queue<Price> price) {
		System.out.println("testing");
		this.twoMA.setShortPrices(price, this.shortDuration);
		this.twoMA.setLongPrices(price, this.longDuration);
		this.decision = this.twoMA.make_decision();
		
		System.out.println(this.decision);
		return this.decision;
	}
}
