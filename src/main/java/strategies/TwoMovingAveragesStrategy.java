package strategies;
//Created by Jack Guo as part of group assignment with Daniel Deng

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import etcs.Price;

public class TwoMovingAveragesStrategy {
    public MovingAverages short_prices;
    public MovingAverages long_prices;
    public double long_average;
    public double short_average;
    // 0 = neutral, 1=short, 2=long
    public static int status;
    
    //Empty constructor to reset status
    public TwoMovingAveragesStrategy(){
        status = 0;
    }
    
    //Set short prices with initial list of prices and period
    public void setShortPrices(Queue<Price> ini, int period){
        short_prices = new MovingAverages(ini, period); 
        try {
			short_average = short_prices.calcMA ();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    //Set short prices with no initial list of prices
    public void setShortPrices(int period){
        short_prices = new MovingAverages(period); 
    }
    
    public void setLongPrices(Queue<Price> ini, int period){
        long_prices = new MovingAverages(ini, period);
        try {
			long_average = long_prices.calcMA ();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public void setLongPrices(int period){
        long_prices = new MovingAverages(period);
    }
    
//    //Tracks and updates long and short prices with incoming tag and ticker
//    public String track(double t, String tag){
//        try{
//            if(tag.equals ("long")){
//                long_average = long_prices.addData(t);
//            }else if(tag.equals ("short")){
//                short_average = short_prices.addData(t);
//            }else{
//                return "Incorrect Tag";
//            }
//            
//            //Make decision based on current averages
//            return make_decision();
//        }catch(Exception e){
//            return "Well someone goof'd";
//        }
//    }
    
    //Logic for making decisions
    public String make_decision(){
        if(short_average  < 0 || long_average < 0 ){        
        return "Not enough prices";}
        
        //Initial. If short prices > long prices buy and set status, vice versa
        if(status==0){
            if(short_average > long_average){
                status = 1;
                return "Buy";
            }
            else{
                status = 2;
                return "Sell";
            }
        }else{
            //Check which average is greater, check status of status and update/return respectively
            if(long_average > short_average){
                if(status == 2){
                    return "Do nothing";
                }else{
                    status = 2;
                    return "Sell";
                }
         
            }
            else{
                if(status == 1){
                    return "Do nothing";
                    
                }
                else{
                    status = 1;
                    return "Buy";
                }
            }
            
        }
    }
}

