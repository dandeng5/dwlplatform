package strategies;
import java.util.*;

import etcs.Price;


public class MovingAverages {
    int period;
    Queue<Price> dataStorage = new LinkedList<Price>();
    
    public MovingAverages(int initialPeriod){
        period = initialPeriod;
    }
    
    public MovingAverages(Queue<Price> initialData, int initialPeriod) {
        dataStorage = initialData;
        period = initialPeriod;
        
    }
    
//    public double addData(double data) throws Exception {
//        if (dataStorage.size() < period) {
//            dataStorage.add (data);
//            
//            if (dataStorage.size () >= period) {
//                try {
//                	return calcMA();
//                } catch (Exception e) {
//                	throw e;
//                }
//            }
//            else {
//                return -1;
//            }
//        }
//        else {
//            dataStorage.remove();
//            dataStorage.add (data);
//            try {
//            	return calcMA();
//            } catch (Exception e) {
//            	throw e;
//            }
//        }
//    }
    
    public double calcMA() throws Exception {
            Double count = (double) 0;
            for (int i=0; i<this.period; i++) {
            	try {
            	double average = (((LinkedList<Price>) dataStorage).get(i).low + ((LinkedList<Price>) dataStorage).get(i).high)/2;
                count += average;
            	} catch (Exception e) {
                    System.out.println("Average has been asked too soon");
                    throw e;
            	}
                //System.out.println (count);
            }
            //System.out.println ();
            //System.out.println ((Math.round((count/period) * 10.0)/10.0));
            System.out.println(Math.round((count/period) * 100.0)/100.0);
            return Math.round((count/period) * 100.0)/100.0;

    }
    
    
    
    public static void main(String[] args) throws Exception {
        Queue<Price> sampleData = new LinkedList<Price>();
        Price p = new Price("lol", 3, 4, 2.45, 5, 100);
        Price s = new Price("wut", 4, 5, 4.35, 6, 200);
//        sampleData.add(2.0);
//        sampleData.add(4.7);
//        sampleData.add(2.9);
//        sampleData.add(1.1);
//        sampleData.add(0.8);
//        sampleData.add(4.4);
        sampleData.add(p);
        sampleData.add(s);
        
        MovingAverages test = new MovingAverages(sampleData, 2);
        System.out.println(test.calcMA());
    }
    
}

