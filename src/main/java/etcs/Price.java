package etcs;

public class Price {
	public String timestamp;
	public double open;
	public double high;
	public double low;
	public double close;
	public int volume;
	
	
	public Price(String timestamp, double open, double high, double low, double close, int volume) {
		super();
		this.timestamp = timestamp;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.volume = volume;
	}


	public String getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


	public double getOpen() {
		return open;
	}


	public void setOpen(double open) {
		this.open = open;
	}


	public double getHigh() {
		return high;
	}


	public void setHigh(double high) {
		this.high = high;
	}


	public double getLow() {
		return low;
	}


	public void setLow(double low) {
		this.low = low;
	}


	public double getClose() {
		return close;
	}


	public void setClose(double close) {
		this.close = close;
	}


	public int getVolume() {
		return volume;
	}


	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	public String toString() {
		return "Time:"+this.getTimestamp()+"\nOpen:"+this.getOpen()+"\nClose:"+this.getClose()+"\nHigh:"+this.getHigh()+
				"\nLow:"+this.getLow()+"\nVolume:"+this.getVolume();
	}
	
	
}
