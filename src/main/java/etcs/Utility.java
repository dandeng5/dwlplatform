package etcs;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import strategies.TradingStrategy;
import strategies.TradingStrategyHandler;

public class Utility {
	public String buy(double price, int size, String symbol, int id) {
		String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		String builder = "<trade>\n"+"  <buy>true</buy>\n"+"  <id>"+id+"</id>\n"+"  <price>"+price+"</price>\n"+"  <size>"+size+"</size>\n"
				+"  <stock>"+symbol+"</stock>\n"+"  <whenAsDate>"+time+"</whenAsDate>\n"+"</trade>";
		return builder;
	}
	public String sell(double price, int size, String symbol, int id) {
		String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		String builder = "<trade>\n"+"  <buy>false</buy>\n"+"  <id>"+id+"</id>\n"+"  <price>"+price+"</price>\n"+"  <size>"+size+"</size>\n"
				+"  <stock>"+symbol+"</stock>\n"+"  <whenAsDate>"+time+"</whenAsDate>\n"+"</trade>";
		return builder;
	}
	public Trade verify(String msg) {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder;
	    int value = 0;
		try {
			builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(msg));
			Document doc = builder.parse(is);
			doc.getDocumentElement().normalize();
			Element eResult = (Element) doc.getElementsByTagName("result").item(0);
			Element ePrice = (Element) doc.getElementsByTagName("price").item(0);
			Element eSize = (Element) doc.getElementsByTagName("size").item(0);
			Element eDate = (Element) doc.getElementsByTagName("whenAsDate").item(0);
			Element eStock = (Element) doc.getElementsByTagName("stock").item(0);
			Element eBuy = (Element) doc.getElementsByTagName("buy").item(0);
			Trade tmp = new Trade(doc.getElementsByTagName("whenAsDate").item(0).getTextContent(), 
					doc.getElementsByTagName("stock").item(0).getTextContent(), 
					doc.getElementsByTagName("buy").item(0).getTextContent(),
					Double.parseDouble(doc.getElementsByTagName("price").item(0).getTextContent()),
					Integer.parseInt(doc.getElementsByTagName("size").item(0).getTextContent()),
					doc.getElementsByTagName("result").item(0).getTextContent());
			System.out.println(doc.getElementsByTagName("stock").item(0).getTextContent());
			return tmp;

			//FILLED
			//PARTIALLY_FILLED
			//REJECTED
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Trade("1", "1", "1", 1, 1, "1");
		}
		catch(Exception e) {
			e.printStackTrace();
			return new Trade("1", "1", "1", 1, 1, "1");
		}

		
	}
	public Price parse(String line) {
		
			String [] cell = line.split(",");
			Price tmp = new Price(cell[0], Double.parseDouble(cell[1]), Double.parseDouble(cell[2]), Double.parseDouble(cell[3]), Double.parseDouble(cell[4]), Integer.parseInt(cell[5]));		
			return tmp;	
	}
	public Queue<Price> download(String ticker, int periods) {
        Queue<Price> q = new LinkedList<>();
		try {
            URL url = new URL("http://incanada1.conygre.com:9080/prices/"+ticker+"?periods="+periods);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            System.out.println("Connected :)");
            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            int flag = 0;
            while ((line = reader.readLine()) != null) {
                if(flag!=0) {
            	System.out.println(line);
            	Price tmp = parse(line);
            	q.add(tmp);
            	//System.out.println(tmp);   	
                }
                flag =1;
            }
            return q;
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return q;
	}
	public Queue<Price> update(String ticker, int periods, Queue<Price> old) {
		Queue<Price> q = new LinkedList<Price>();
        String timestamp = "";
        int flag = 0;
        int readflag = 0;
		try {
			URL url = new URL("http://incanada1.conygre.com:9080/prices/"+ticker+"?periods="+periods);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            System.out.println("Connected :)");
            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;         
            while(old.size() > 0)
            {
            	if(!(old.size()>1)) {
            		Price tmp = old.poll();
            		timestamp = tmp.timestamp;
                q.add(tmp);
            	}
            	else {
                q.add(old.poll());
            	}
            }
            old.clear();
            while ((line = reader.readLine()) != null) {
                if(flag!=0) {
                	System.out.println(line);
                	Price tmp = parse(line);
                	if(readflag==1) {
                    	q.add(tmp);
                	}
                	if(tmp.timestamp.equals(timestamp)) {
                		readflag = 1;
                	}
                	//System.out.println(tmp);   	
                }
                flag =1;            
            }
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return q;
	}
	
	public static int loop(String ticker) {
		int flag = 0;
		Utility dd = new Utility();
		Queue<Price> haha = dd.download(ticker,50);
		TradingStrategy t = new TradingStrategy("TwoMA", haha, 5, 20);
		t.executeTwoMA(haha);
		//System.out.println(dd.buy(12, 12, ticker,1));
		while(flag == 0) {	
		//System.out.println(haha);
		try {
			TimeUnit.SECONDS.sleep(15);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		haha = dd.update(ticker,50,haha);
		t.executeTwoMA(haha);
		//System.out.println(haha);
		}
		return 0;
		
		//fuck
	}
	
	public static void main(String[] args) {
		Utility dd = new Utility();
		//dd.verify(dd.buy(12, 12, "AAPL",1));
		dd.verify("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><trade><buy>true</buy><id>0</id><price>110.9966</price><result>REJECTED</result><size>0</size><stock>AAPL</stock><whenAsDate>2018-07-19T20:47:28.509Z</whenAsDate></trade>");
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		TradingStrategyHandler me = new TradingStrategyHandler(0, 0, "AAPL", 50);
		CompletableFuture.supplyAsync(() -> me.loop(), executorService).thenRun(() -> { executorService.shutdown(); });
		try {
			TimeUnit.SECONDS.sleep(15);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		me.setFlag(1);
		/*
		Utility dd = new Utility();
		System.out.println(dd.buy(12, 12, "AAPL",1));
		Queue<Price> haha = dd.download("C",50);
		System.out.println(haha);
		try {
			TimeUnit.SECONDS.sleep(15);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(hiahia);
		CompletableFuture.supplyAsync(() -> loop("AAPL"), executorService).thenRun(() -> { executorService.shutdown(); });
		CompletableFuture.supplyAsync(() -> loop("C"), executorService).thenRun(() -> { executorService.shutdown(); });
		CompletableFuture.supplyAsync(() -> loop("BABA"), executorService).thenRun(() -> { executorService.shutdown(); });
		CompletableFuture.supplyAsync(() -> loop("IQ"), executorService).thenRun(() -> { executorService.shutdown(); });
		CompletableFuture.supplyAsync(() -> loop("IQ"), executorService).thenRun(() -> { executorService.shutdown(); });
		 */
		executorService.shutdown();
		
	}
}
