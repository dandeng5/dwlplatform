package messaging;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import etcs.Utility;
//import cc.beans.MyMessageHandler;

public class BrokerMessageHandler {
	private ApplicationContext context;
	private JmsTemplate jmsTemplate;
	private Destination destination;
	private Destination replyTo;
	
	private BrokerMessageHandler() {
		context = new ClassPathXmlApplicationContext("broker-config.xml");
		destination =  context.getBean("destination", Destination.class);
		replyTo =  context.getBean("replyTo", Destination.class);
		jmsTemplate = (JmsTemplate) context.getBean("messageSender");
	}


	public static void main(String[] args) {
		BrokerMessageHandler mmh = new BrokerMessageHandler();
		mmh.run();
	}

	private void run() {
		makeMessage(destination);
		System.out.println("Message Sent to JMS Queue:- ");

	}

	private void makeMessage(final Destination destination) {
		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				MapMessage message = session.createMapMessage();
				Utility utl = new Utility();
				message.setString(utl.buy(12.3, 10000, "AAPL", 1),"Trade");
				message.setJMSReplyTo(replyTo);
				message.setJMSCorrelationID("12399999");
				return message;
			}
		});

	}
}
