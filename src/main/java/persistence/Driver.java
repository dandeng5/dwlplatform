package persistence;


import javax.persistence.*;
import javax.persistence.EntityManagerFactory;



/**
 * @author Administrator
 *
 */
public class Driver {
	private EntityManagerFactory factory;
	private EntityManager em;

	private Driver() {
		factory = Persistence.createEntityManagerFactory("Demo");
		em = factory.createEntityManager();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Driver dd = new Driver();
		dd.run();
	}

	private void run() {
		//Address address = new Address(1L,"123 Oka Road", "Miss", "Ontario");
		//Person charlotte = new Person(1L,"Charlotte", "Winter", 42, address);
		//System.out.println( charlotte );
		StrategyType ss = new StrategyType(2, "Helloworld");
		StrategyType p = new StrategyType(2, "wow");
		Strategy s = new Strategy("lool", 1, 2, 3, 4, 124532);
		s.setStrategyType(p);
		
		try {
			em.getTransaction().begin();
//			em.persist(ss);
			em.persist(p);
			em.persist(s);
			em.getTransaction().commit();
		} catch(Exception e) {
			System.out.println("Failed to add new objects to database");
		}
		finally {
			factory.close();
		}
		
	}
}