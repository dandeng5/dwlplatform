package persistence;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "STRATEGYTYPE")
public class StrategyType implements Serializable{
	@Id 
	@Column(name = "strategyid")
	private int strategyId;
	
	@Column(name = "name")
	private String name;
	
	public StrategyType() {
		
	}
	/**
	 * @param id
	 * @param name
	 */
	public StrategyType(int id, String name) {
		super();
		setId(id);
		setName(name);
	}
	public int getId() {
		return strategyId;
	}
	
	public void setId(int id) {
		this.strategyId = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
