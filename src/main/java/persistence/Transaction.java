package persistence;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRANSACTION")
public class Transaction implements Serializable{
	@Id
	private int id;
	private int position;
	private Double price;
	private int quantity;
	private int strategy;
	private String symbol;
	private int timeplaced;
	
	public Transaction() {
		
	}
	/**
	 * @param id
	 * @param position
	 * @param price
	 * @param quantity
	 * @param strategy
	 * @param symbol
	 * @param timeplaced
	 */
	public Transaction(int id, int position, Double price, int quantity, int strategy, String symbol, int timeplaced) {
		super();
		this.id = id;
		this.position = position;
		this.price = price;
		this.quantity = quantity;
		this.strategy = strategy;
		this.symbol = symbol;
		this.timeplaced = timeplaced;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getStrategy() {
		return strategy;
	}
	public void setStrategy(int strategy) {
		this.strategy = strategy;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public int getTimeplaced() {
		return timeplaced;
	}
	public void setTimeplaced(int timeplaced) {
		this.timeplaced = timeplaced;
	}
	
	
	
	
}
