package persistence;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "STRATEGY")
public class Strategy implements Serializable {
	
	@Id @GeneratedValue(strategy = GenerationType.TABLE) @Column(name="id")
    private int id;
   
	@Column(name = "NAME")
    private String name;
	
	@Column(name = "TIMECREATED")
	private int timecreated;
	
//	@Column(name = "TYPE")
//    private int type;
	
	@Column(name = "PAR1")
    private int par1;
	
	@Column(name="PAR2")
    private int par2;
	
	@Column(name="PAR3")
    private int par3;
	
	@Column(name="PAR4")
    private int par4;
	
	@Column(name = "type")
	private StrategyType type;
	
    
    public Strategy() {
    	
    }
    

	public Strategy(String name, int par1, int par2, int par3, int par4, int timecreated) {
		super();
		this.name = name;
		this.timecreated = timecreated;
		this.par1 = par1;
		this.par2 = par2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTimecreated() {
		return timecreated;
	}

	public void setTimecreated(int timecreated) {
		this.timecreated = timecreated;
	}

//	public int getType() {
//		return type;
//	}
//
//	public void setType(int type) {
//		this.type = type;
//	}

	public int getPar1() {
		return par1;
	}

	public void setPar1(int par1) {
		this.par1 = par1;
	}

	public int getPar3() {
		return par3;
	}

	public void setPar3(int par3) {
		this.par3 = par3;
	}
	
	public int getPar4() {
		return par4;
	}

	public void setPar4(int par4) {
		this.par4 = par4;
	}

	
	@ManyToOne
	@JoinColumn(name = "strategyid")
	public StrategyType getStrategyType() {
		return type;
	}
	
	public void setStrategyType(StrategyType st) {
		this.type = st;
	}

}
